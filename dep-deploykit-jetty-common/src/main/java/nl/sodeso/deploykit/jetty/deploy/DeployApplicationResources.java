package nl.sodeso.deploykit.jetty.deploy;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.commons.token.TokenReplacingReader;
import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.configuration.JettyConfUtil;
import nl.sodeso.deploykit.jetty.configuration.JettyConfigurationException;
import nl.sodeso.deploykit.jetty.util.SystemPropertiesContainer;
import nl.sodeso.deploykit.service.model.Profile;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployApplicationResources {

    private static final Logger LOG = Logger.getLogger(DeployApplicationResources.class.getName());

    /**
     * Deploys the application resources to a JBoss module.
     * @param profile the profile.
     */
    public static void deploy(@Nonnull Profile profile) {
        File resources = getDeployKitResourcesFolder();
        loopFilesInFolder(resources, profile);
    }

    /**
     * Loops through all the resources recursivly and processes every single file found.
     * @param folder the folder containing all the resources.
     * @param profile the profile containing all the placeholders.
     */
    private static void loopFilesInFolder(@Nonnull File folder, @Nonnull Profile profile) {
        try {
            Files.walkFileTree(folder.toPath(), new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    processResource(file.toFile(), profile);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            // Do nothing.
        }
    }

    /**
     * Processes a single resource file (replaces all placeholders with their actual value), and it will write it to the
     * destination folder within the JBoss modules folder along with the reconstructed path.
     *
     * @param resource the resource to process.
     * @param profile the profile containing all the placeholders that are available.
     */
    private static void processResource(@Nonnull File resource, @Nonnull Profile profile) {
        LOG.log(Level.INFO, "Processing resource: " + resource.getPath());

        TokenReplacingReader reader = null;
        OutputStreamWriter writer = null;

        try {
            reader = new TokenReplacingReader(new InputStreamReader(new FileInputStream(resource)), profile);

            // Get the base folder where the resources are located, this will be used to
            // determine which part of the path should be reconstructed within the module.
            Path userdir = new File(System.getProperty("user.dir"), "resources").toPath();

            // Get the path that should be reconstructed within the module (basicly it does
            // a substraction of the working path on the jetty-client side with the additional
            // path within the resource.
            String workingfolder = userdir.relativize(resource.toPath()).toString();

            // Construct the new path based on the JBoss module folder and the additional path
            // constructed before. If the constructed path is empty then just use the JBoss module folder.
            File processedResourceFile;
            if (workingfolder != null && !workingfolder.isEmpty()) {
                processedResourceFile = new File(JettyConfUtil.getJettyResourcesFolder(), workingfolder);
            } else {
                processedResourceFile = JettyConfUtil.getJettyResourcesFolder();
            }

            // Check if the newly constructed path exists, if not create the folders recursively.
            if (!processedResourceFile.getParentFile().exists()) {
                processedResourceFile.getParentFile().mkdirs();
            }

            // Process and write the resource to the destination path.
            LOG.log(Level.INFO, "Writing processed resource: " + processedResourceFile.toString());
            writer = new OutputStreamWriter(new FileOutputStream(processedResourceFile));

            int size;
            char[] buffer = new char[2048];
            while (-1 != (size = reader.read(buffer))) {
                writer.write(buffer, 0, size);
            }

            writer.flush();
        } catch (IOException | JettyConfigurationException e) {
            throw new DeployException(ErrorCode.ERR_017, e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // Do nothing.
                }
            }

            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    // Do nothing.
                }
            }

        }
    }

    /**
     * Returns the resources folder within the DeployKit bin folder.
     * @return the resources folder within the DeployKit bin folder.
     */
    private static File getDeployKitResourcesFolder() {
        File deploykitHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_HOME);
        if (deploykitHomeFolder == null) {
            throw new DeployException(ErrorCode.ERR_004);
        }

        return new File(deploykitHomeFolder, "/bin/resources/");
    }

}
