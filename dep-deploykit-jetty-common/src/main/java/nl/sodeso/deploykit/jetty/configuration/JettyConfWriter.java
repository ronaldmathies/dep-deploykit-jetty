package nl.sodeso.deploykit.jetty.configuration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Ronald Mathies
 */
public class JettyConfWriter {

    public static void write(JettyConfiguration configuration) throws JettyConfigurationException {
        File jettyConfigFile = JettyConfUtil.getJettyConfigFile();
        if (!jettyConfigFile.delete()) {
            throw new JettyConfigurationException("Insufficient privileges to delete the jetty.conf file in the following location '" + jettyConfigFile + "'.");
        }

        try (BufferedWriter br = new BufferedWriter(new FileWriter(jettyConfigFile))) {

            for (JettyXmlFile jettyXmlFile : configuration.getXmlFiles()) {
                br.write(jettyXmlFile.getXmlFileName());
                br.newLine();
            }

            br.flush();
        } catch (IOException e) {
            throw new JettyConfigurationException("Failed to create the jetty.conf file in the following location '" + jettyConfigFile + "'.", e);
        }
    }


}
