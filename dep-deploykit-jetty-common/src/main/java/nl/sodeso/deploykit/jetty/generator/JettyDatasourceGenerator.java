package nl.sodeso.deploykit.jetty.generator;

import freemarker.template.Template;
import nl.sodeso.commons.freemarker.FreemarkerException;
import nl.sodeso.commons.freemarker.FreemarkerUtil;
import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.deploy.DeployException;
import nl.sodeso.deploykit.service.model.datasource.datasource.Datasource;

import javax.annotation.Nonnull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class JettyDatasourceGenerator extends AbstractGenerator {

    private static final Logger LOG = Logger.getLogger(JettyDatasourceGenerator.class.getName());

    public static void generate(@Nonnull Datasource datasource, @Nonnull JettyXmlFile jettyXmlFile) {
        try {
            LOG.log(Level.INFO, "Generating datasource '" + datasource.getLabel() + "'.");
            Template template = FreemarkerUtil.loadTemplate("jetty-datasource.ftl");
            FreemarkerUtil.process(template, datasource, jettyXmlFile.getXmlFile());

            LOG.log(Level.INFO, "Finished generating datasource '" + datasource.getLabel() + "'.");
        } catch (FreemarkerException e) {
            throw new DeployException(ErrorCode.ERR_011, e.getMessage());
        }
    }

}
