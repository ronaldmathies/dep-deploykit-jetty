package nl.sodeso.deploykit.jetty.deploy;

import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.configuration.JettyConfUtil;
import nl.sodeso.deploykit.jetty.configuration.JettyConfiguration;
import nl.sodeso.deploykit.jetty.configuration.JettyConfigurationException;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.generator.JettyDatasourceGenerator;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.Resource;
import nl.sodeso.deploykit.service.model.accesscontrol.DatasourceAccessControl;
import nl.sodeso.deploykit.service.model.datasource.datasource.Datasource;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriverJar;
import org.apache.commons.codec.binary.Base64InputStream;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployDatasourceConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployDatasourceConfiguration.class.getName());

    private static final String ENCODING = "UTF-8";

    private static List<JdbcDriver> previouslyCreatedJdbcDriverModules = new ArrayList<>();

    public static void deploy(@Nonnull JettyConfiguration configuration, @Nonnull Profile profile) {
        for (Datasource datasource : profile.getDatasources()) {
            deployDatasource(configuration, datasource);
        }

        if (profile.getAccessControl() != null && profile.getAccessControl() instanceof DatasourceAccessControl) {
            DatasourceAccessControl datasourceAccessControl = (DatasourceAccessControl)profile.getAccessControl();

            Resource resource = profile.findResourceByUuid(datasourceAccessControl.getDatasource().getUuid(), Datasource.class);
            if (resource == null) {
                deployDatasource(configuration, datasourceAccessControl.getDatasource());
            }

        }
    }

    private static void deployDatasource(@Nonnull JettyConfiguration configuration, @Nonnull Datasource datasource) {
        LOG.log(Level.INFO, "Deploying datasource '" + datasource.getLabel() + "'.");

        deployJdbcDriver(datasource);

        JettyXmlFile jettyXmlFile = new JettyXmlFile(datasource.getUuid(), "ds");
        JettyDatasourceGenerator.generate(datasource, jettyXmlFile);
        configuration.addXmlFile(jettyXmlFile);

        LOG.log(Level.INFO, "Datasource '" + datasource.getLabel() + "' successfully added to Jetty configuration, deployment of the datasource is finished.");
    }

    private static void deployJdbcDriver(@Nonnull Datasource datasource) {
        try {
            // Perform a check to see if we have deployed the JDBC driver while processing a
            // previously data source. If so, then don't add it again since we can re-use the other one.
            if (previouslyCreatedJdbcDriverModules.contains(datasource.getJdbcDriver())) {
                LOG.log(Level.INFO, "Deploying JDBC Driver '" + datasource.getJdbcDriver().getLabel() + "'.");

                List<String> jarFiles = new ArrayList<>();
                for (JdbcDriverJar jdbcDriverJar : datasource.getJdbcDriver().getJdbcDriverJars()) {

                    File jdbcDriverJarFile = new File(JettyConfUtil.getJettyLibFolder(), jdbcDriverJar.getFilename());
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(jdbcDriverJar.getJar().getBytes(Charset.forName(ENCODING)));
                    Files.copy(new Base64InputStream(inputStream, false), jdbcDriverJarFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    jarFiles.add(jdbcDriverJarFile.toString());

                }

                LOG.log(Level.INFO, "Deployment of JDBC Driver '" + datasource.getJdbcDriver().getLabel() + "' successful.");

                previouslyCreatedJdbcDriverModules.add(datasource.getJdbcDriver());
            }
        } catch (IOException | JettyConfigurationException e) {
            throw new DeployException(ErrorCode.ERR_018, e.getMessage());
        }
    }

}
