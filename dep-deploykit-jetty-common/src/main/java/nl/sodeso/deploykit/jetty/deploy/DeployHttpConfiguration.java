package nl.sodeso.deploykit.jetty.deploy;

import nl.sodeso.deploykit.jetty.configuration.JettyConfiguration;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.generator.JettyHttpGenerator;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.connectors.Http;

import javax.annotation.Nonnull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployHttpConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployDatasourceConfiguration.class.getName());

    public static void deploy(@Nonnull JettyConfiguration configuration, @Nonnull Profile profile) {
        if (profile.getHttp() != null) {
            Http http = profile.getHttp();
            LOG.log(Level.INFO, "Deploying HTTP configuration '" + http.getLabel() + "'.");

            JettyXmlFile jettyXmlFile = new JettyXmlFile(http.getUuid(), "http");
            JettyHttpGenerator.generate(http, jettyXmlFile);
            configuration.addXmlFile(jettyXmlFile);

            LOG.log(Level.INFO, "HTTP configuration '" + http.getLabel() + "' successfully added to Jetty configuration, deployment of the HTTP confguration is finished.");
        }
    }

}
