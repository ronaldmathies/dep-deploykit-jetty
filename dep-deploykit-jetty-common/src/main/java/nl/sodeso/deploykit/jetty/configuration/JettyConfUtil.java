package nl.sodeso.deploykit.jetty.configuration;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.deploy.DeployException;
import nl.sodeso.deploykit.jetty.util.SystemPropertiesContainer;

import java.io.File;

/**
 * @author Ronald Mathies
 */
public class JettyConfUtil {

    public static File getJettyHomeFolder() {
        File jettyHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.JETTY_HOME);
        if (jettyHomeFolder == null) {
            throw new DeployException(ErrorCode.ERR_005);
        }

        return jettyHomeFolder;
    }

    public static File getJettyConfigFile() throws JettyConfigurationException {
        File jettyConfFile = new File(JettyConfUtil.getJettyHomeFolder(), "/etc/etc.conf");
        if (!jettyConfFile.exists()) {
            throw new JettyConfigurationException("Could not find the jetty.conf file in the following location '" + jettyConfFile + "'.");
        }
        return jettyConfFile;
    }

    public static File getJettyEtcFolder() throws JettyConfigurationException {
        File jettyEtcFolder = new File(JettyConfUtil.getJettyHomeFolder(), "/etc");
        if (!jettyEtcFolder.exists()) {
            throw new JettyConfigurationException("Could not find the jetty /etc folder.");
        }
        return jettyEtcFolder;
    }

    public static File getJettyResourcesFolder() throws JettyConfigurationException {
        File jettyResourcesFolder = new File(JettyConfUtil.getJettyHomeFolder(), "/resources");
        if (!jettyResourcesFolder.exists()) {
            throw new JettyConfigurationException("Could not find the jetty /resources folder.");
        }
        return jettyResourcesFolder;
    }

    public static File getJettyLibFolder() throws JettyConfigurationException {
        File jettyLibFolder = new File(JettyConfUtil.getJettyHomeFolder(), "/lib");
        if (!jettyLibFolder.exists()) {
            throw new JettyConfigurationException("Could not find the jetty /lib folder.");
        }
        return jettyLibFolder;
    }

}
