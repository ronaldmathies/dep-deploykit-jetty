package nl.sodeso.deploykit.jetty.configuration;

/**
 * Exception is thrown when there was a failure reading the jetty.conf file.
 *
 * @author Ronald Mathies
 */
public class JettyConfigurationException extends Exception {

    public JettyConfigurationException() {
    }

    public JettyConfigurationException(String message) {
        super(message);
    }

    public JettyConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public JettyConfigurationException(Throwable cause) {
        super(cause);
    }

    public JettyConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
