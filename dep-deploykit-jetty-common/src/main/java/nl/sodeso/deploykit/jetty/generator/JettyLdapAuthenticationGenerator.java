package nl.sodeso.deploykit.jetty.generator;

import freemarker.template.Template;
import nl.sodeso.commons.freemarker.FreemarkerException;
import nl.sodeso.commons.freemarker.FreemarkerUtil;
import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.configuration.JettyConfUtil;
import nl.sodeso.deploykit.jetty.configuration.JettyConfigurationException;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.deploy.DeployException;
import nl.sodeso.deploykit.service.model.accesscontrol.LdapAccessControl;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class JettyLdapAuthenticationGenerator extends AbstractGenerator {

    private static final Logger LOG = Logger.getLogger(JettyLdapAuthenticationGenerator.class.getName());

    public static void generate(@Nonnull LdapAccessControl ldapAccessControl, @Nonnull JettyXmlFile jettyXmlFile) {
        try {
            LOG.log(Level.INFO, "Generating LDAP authentication configuration configuration '" + ldapAccessControl.getLabel() + "'.");

            Template template = FreemarkerUtil.loadTemplate("login-conf.ftl");
            FreemarkerUtil.process(template, ldapAccessControl, new File(JettyConfUtil.getJettyEtcFolder(), "login.conf"));

            template = FreemarkerUtil.loadTemplate("jetty-ldap.ftl");
            FreemarkerUtil.process(template, ldapAccessControl, jettyXmlFile.getXmlFile());

            LOG.log(Level.INFO, "Finished generating LDAP authentication configuration '" + ldapAccessControl.getLabel() + "'.");

        } catch (FreemarkerException | JettyConfigurationException e) {
            throw new DeployException(ErrorCode.ERR_016, e.getMessage());
        }
    }
}
