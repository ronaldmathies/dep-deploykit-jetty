package nl.sodeso.deploykit.jetty.generator;

import freemarker.template.Template;
import nl.sodeso.commons.freemarker.FreemarkerException;
import nl.sodeso.commons.freemarker.FreemarkerUtil;
import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.deploy.DeployException;
import nl.sodeso.deploykit.service.model.connectors.Https;
import org.apache.commons.codec.binary.Base64InputStream;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class JettyHttpsGenerator extends AbstractGenerator {

    private static final Logger LOG = Logger.getLogger(JettyHttpsGenerator.class.getName());

    private static final String ENCODING = "UTF-8";

    public static void generate(@Nonnull Https https, @Nonnull JettyXmlFile jettyXmlFile) {
        try {
            LOG.log(Level.INFO, "Generating SSL/HTTPS configuration '" + https.getLabel() + "'.");
            Template template = FreemarkerUtil.loadTemplate("jetty-https.ftl");
            FreemarkerUtil.process(template, https, jettyXmlFile.getXmlFile());

            LOG.log(Level.INFO, "Finished generating SSL/HTTPS configuration '" + https.getLabel() + "'.");

            try {
                LOG.log(Level.INFO, "Generating SSL/HTTPS keystore '" + https.getLabel() + "'.");
                File outputFile = createOutputFile(https.getUuid(), "keystore");

                ByteArrayInputStream inputStream = new ByteArrayInputStream(https.getKeystore().getBytes(Charset.forName(ENCODING)));
                Files.copy(new Base64InputStream(inputStream, false), outputFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                LOG.log(Level.INFO, "Finished generating SSL/HTTPS keystore '" + https.getLabel() + "'.");
            } catch (IOException e) {
                throw new DeployException(ErrorCode.ERR_014, e.getMessage());
            }

        } catch (FreemarkerException e) {
            throw new DeployException(ErrorCode.ERR_013, e.getMessage());
        }
    }
}
