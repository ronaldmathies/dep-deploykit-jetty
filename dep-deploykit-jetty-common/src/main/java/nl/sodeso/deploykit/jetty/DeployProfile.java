package nl.sodeso.deploykit.jetty;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.jetty.configuration.JettyConfReader;
import nl.sodeso.deploykit.jetty.configuration.JettyConfWriter;
import nl.sodeso.deploykit.jetty.configuration.JettyConfiguration;
import nl.sodeso.deploykit.jetty.configuration.JettyConfigurationException;
import nl.sodeso.deploykit.jetty.deploy.*;
import nl.sodeso.deploykit.jetty.util.SystemPropertiesContainer;
import nl.sodeso.deploykit.service.client.DeployKitServiceClient;
import nl.sodeso.deploykit.service.client.exceptions.ProfileNotFoundException;
import nl.sodeso.deploykit.service.client.exceptions.ProfileRetreivalException;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.util.FailedToWriteProfileException;
import nl.sodeso.deploykit.service.util.ProfileStore;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployProfile {

    private static final Logger LOG = Logger.getLogger(DeployProfile.class.getName());

    public static void deploy() {
        String deployKitServiceHost;
        Integer deployKitServicePort;
        String deployKitDeployProfile = null;
        String deployKitDeployProfileVersion;

        try {
            deployKitDeployProfile = PropertyConfiguration.getInstance().getStringProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_DEPLOY_PROFILE, null);
            if (deployKitDeployProfile == null) {
                throw new DeployException(ErrorCode.ERR_003);
            }

            deployKitServiceHost = PropertyConfiguration.getInstance().getStringProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_SERVICE_HOST, null);
            if (deployKitServiceHost == null) {
                throw new DeployException(ErrorCode.ERR_001);
            }

            deployKitServicePort = PropertyConfiguration.getInstance().getIntegerProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_SERVICE_PORT, null);
            if (deployKitServicePort == null) {
                throw new DeployException(ErrorCode.ERR_002);
            }

            deployKitDeployProfileVersion = PropertyConfiguration.getInstance().getStringProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_DEPLOY_PROFILE_VERSION);
            if (deployKitDeployProfileVersion == null) {
                throw new DeployException(ErrorCode.ERR_015);
            }

            LOG.log(Level.INFO, "Reading existing Jetty configuration.");
            JettyConfiguration jettyConfiguration = JettyConfReader.read();

            LOG.log(Level.INFO, String.format("DeployKit Service: %s:%d", deployKitServiceHost, deployKitServicePort));
            Profile profile = new DeployKitServiceClient(deployKitServiceHost, deployKitServicePort).findProfile(deployKitDeployProfile, deployKitDeployProfileVersion);
            DeployDatasourceConfiguration.deploy(jettyConfiguration, profile);
            DeployAuthenticationConfiguration.deploy(jettyConfiguration, profile);
            DeployHttpConfiguration.deploy(jettyConfiguration, profile);
            DeployHttpsConfiguration.deploy(jettyConfiguration, profile);

            LOG.log(Level.INFO, "Writing new Jetty configuration.");
            JettyConfWriter.write(jettyConfiguration);

            ProfileStore.write(getProfilesFolder(), profile);
        } catch (JettyConfigurationException e) {
            throw new DeployException(ErrorCode.ERR_006, e.getMessage());
        } catch (ProfileNotFoundException e) {
            throw new DeployException(ErrorCode.ERR_007, deployKitDeployProfile, e.getMessage());
        } catch (ProfileRetreivalException e) {
            throw new DeployException(ErrorCode.ERR_008, deployKitDeployProfile, e.getMessage());
        } catch (FailedToWriteProfileException e) {
            throw new DeployException(ErrorCode.ERR_009, deployKitDeployProfile, e.getMessage());
        }
    }

    private static File getProfilesFolder() {
        File deploykitHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.DEPLOYKIT_HOME, null);
        if (deploykitHomeFolder == null) {
            throw new DeployException(ErrorCode.ERR_004);
        }

        File file = new File(deploykitHomeFolder, "/bin/profiles/");
        if (!file.exists()) {
            file.mkdirs();
        }

        return file;
    }
}
