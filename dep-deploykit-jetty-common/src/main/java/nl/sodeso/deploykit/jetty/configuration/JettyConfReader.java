package nl.sodeso.deploykit.jetty.configuration;

import java.io.*;

/**
 * @author Ronald Mathies
 */
public class JettyConfReader {

    private static final String COMMENT_LINE = "#";
    private static final String XML_FILE = ".xml";

    public static JettyConfiguration read() throws JettyConfigurationException {
        JettyConfiguration confguration = new JettyConfiguration();

        File jettyConfigFile = JettyConfUtil.getJettyConfigFile();
        try (BufferedReader br = new BufferedReader(new FileReader(jettyConfigFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                processLine(confguration, line);
            }

            return confguration;
        } catch (FileNotFoundException e) {
            throw new JettyConfigurationException("Could not find the jetty.conf file in the following location '" + jettyConfigFile + "'.", e);
        } catch (IOException e) {
            throw new JettyConfigurationException("Exception while reading the jetty.conf file from the following location '" + jettyConfigFile + "'.", e);
        }
    }

    private static void processLine(JettyConfiguration configuration, String line) {
        if (line != null && !line.trim().isEmpty()) {
            if (line.startsWith(COMMENT_LINE)) {
                // ignore
            } else if (line.endsWith(XML_FILE)) {
                configuration.addXmlFile(new JettyXmlFile(line));
            }
        }
    }

}
