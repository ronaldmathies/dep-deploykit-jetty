package nl.sodeso.deploykit.jetty.generator;

import nl.sodeso.commons.properties.PropertyConfiguration;
import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.deploy.DeployException;
import nl.sodeso.deploykit.jetty.util.SystemPropertiesContainer;

import javax.annotation.Nonnull;
import java.io.File;

/**
 * @author Ronald Mathies
 */
public class AbstractGenerator {

    public static File createOutputFile(@Nonnull String uuid, @Nonnull String extension) {
        File jettyHomeFolder = PropertyConfiguration.getInstance().getFileProperty(SystemPropertiesContainer.DOMAIN, SystemPropertiesContainer.JETTY_HOME, null);
        if (jettyHomeFolder == null) {
            throw new NullPointerException("The jetty.configuration.folder has not been setup correctly, please enter a correct path.");
        }

        File jettyEtcFolder = new File(jettyHomeFolder, "/etc/");
        if (!jettyEtcFolder.exists()) {
            if (!jettyEtcFolder.mkdirs()) {
                throw new DeployException(ErrorCode.ERR_010, jettyEtcFolder.toString());
            }
        }

        return new File(jettyEtcFolder, uuid + "." + extension);
    }

}
