package nl.sodeso.deploykit.jetty;

import nl.sodeso.deploykit.jetty.util.SystemPropertiesContainer;

/**
 * @author Ronald Mathies
 */
public enum ErrorCode {

    ERR_000("ERR-000", "An unexpected error occurred: %s."),
    ERR_001("ERR-001", "System environmental variable " + SystemPropertiesContainer.DEPLOYKIT_SERVICE_HOST + " was not set."),
    ERR_002("ERR-002", "System environmental variable " + SystemPropertiesContainer.DEPLOYKIT_SERVICE_PORT + " was not set."),
    ERR_003("ERR-003", "System environmental variable " + SystemPropertiesContainer.DEPLOYKIT_DEPLOY_PROFILE + " was not set."),
    ERR_004("ERR-004", "System environmental variable " + SystemPropertiesContainer.DEPLOYKIT_HOME + " was not set."),
    ERR_005("ERR-005", "System environmental variable " + SystemPropertiesContainer.JETTY_HOME + " was not set."),
    ERR_006("ERR-006", "Failed to load / write new Jetty configuration, error message: %s"),
    ERR_007("ERR-007", "Profile '%s' not found, error message: %s"),
    ERR_008("ERR-008", "Failed to retrieve profile '%s', error message: %s"),
    ERR_009("ERR-009", "Failed to write profile '%s' to profiles folder, error message: %s"),
    ERR_010("ERR-010", "Failed to create target folder '%s'."),
    ERR_011("ERR-011", "Failed to generate data source, error message: %s"),
    ERR_012("ERR-012", "Failed to generate http configuration, error message: %s"),
    ERR_013("ERR-013", "Failed to generate https configuration, error message: %s"),
    ERR_014("ERR-014", "Failed to generate https keystore, error message: %s"),
    ERR_015("ERR-015", "System environmental variable " + SystemPropertiesContainer.DEPLOYKIT_DEPLOY_PROFILE_VERSION + " was not set."),
    ERR_016("ERR-016", "Failed to generate LDAP authentication configuration, error message: '%s'"),
    ERR_017("ERR-017", "Failed to generate application resources configuration, error message: '%s'"),
    ERR_018("ERR-018", "Failed to store JDBC drivers during data source deployment, error message: %s");


    String code;
    String description;

    ErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

}
