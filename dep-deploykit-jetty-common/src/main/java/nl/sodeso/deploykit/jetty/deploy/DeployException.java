package nl.sodeso.deploykit.jetty.deploy;

import nl.sodeso.deploykit.jetty.ErrorCode;

/**
 * @author Ronald Mathies
 */
public class DeployException extends RuntimeException {

    /**
     * Constructs a DeployException.
     * @param code the error code.
     * @param args the arguments for the message.
     */
    public DeployException(ErrorCode code, String ... args) {
        super(code.getCode() + " - " + String.format(code.getDescription(), (Object[])args));
    }

}
