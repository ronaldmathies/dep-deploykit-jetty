package nl.sodeso.deploykit.jetty.deploy;

import nl.sodeso.deploykit.jetty.configuration.JettyConfiguration;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.generator.JettyHttpsGenerator;
import nl.sodeso.deploykit.service.model.Profile;
import nl.sodeso.deploykit.service.model.connectors.Https;

import javax.annotation.Nonnull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class DeployHttpsConfiguration {

    private static final Logger LOG = Logger.getLogger(DeployDatasourceConfiguration.class.getName());

    public static void deploy(@Nonnull JettyConfiguration configuration, @Nonnull Profile profile) {
        if (profile.getHttps() != null) {
            Https https = profile.getHttps();
            LOG.log(Level.INFO, "Deploying SSL/HTTPS configuration '" + https.getLabel() + "'.");

            JettyXmlFile jettyXmlFile = new JettyXmlFile(https.getUuid(), "ssl");
            JettyHttpsGenerator.generate(https, jettyXmlFile);
            configuration.addXmlFile(jettyXmlFile);

            LOG.log(Level.INFO, "SSL/HTTPS configuration '" + https.getLabel() + "' successfully added to Jetty configuration, deployment of the SSL/HTTPS confguration is finished.");
        }
    }

}
