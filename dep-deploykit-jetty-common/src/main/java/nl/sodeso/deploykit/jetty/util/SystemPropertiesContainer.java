package nl.sodeso.deploykit.jetty.util;

import nl.sodeso.commons.properties.annotations.Resource;
import nl.sodeso.commons.properties.annotations.SystemResource;
import nl.sodeso.commons.properties.containers.system.SystemContainer;

/**
 * Properties Container for resolving system properties.
 *
 * @author Ronald Mathies
 */
@Resource(domain="default")
@SystemResource
public class SystemPropertiesContainer extends SystemContainer {

    public static final String DOMAIN = "default";

    public static final String DEPLOYKIT_HOME = "DEPLOYKIT_HOME";
    public static final String JETTY_HOME = "JETTY_HOME";
    public static final String DEPLOYKIT_SERVICE_HOST = "DEPLOYKIT_SERVICE_HOST";
    public static final String DEPLOYKIT_SERVICE_PORT = "DEPLOYKIT_SERVICE_PORT";
    public static final String DEPLOYKIT_DEPLOY_PROFILE = "DEPLOYKIT_DEPLOY_PROFILE";
    public static final String DEPLOYKIT_DEPLOY_PROFILE_VERSION= "DEPLOYKIT_DEPLOY_PROFILE_VERSION";

}
