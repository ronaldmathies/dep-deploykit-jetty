package nl.sodeso.deploykit.jetty.configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ronald Mathies
 */
public class JettyConfiguration {

    private List<JettyXmlFile> xmlFiles = new ArrayList<>();

    public JettyConfiguration() {}

    public void addXmlFile(JettyXmlFile jettyXmlFile) {
        xmlFiles.add(jettyXmlFile);
    }

    public List<JettyXmlFile> getXmlFiles() {
        return this.xmlFiles;
    }

    public JettyXmlFile getXmlFileForUuid(String uuid, String suffix) {
        for (JettyXmlFile jettyXmlFile : xmlFiles) {
            if (jettyXmlFile.isEqual(uuid, suffix)) {
                return jettyXmlFile;
            }
        }

        return null;
    }

    public void removeXmlFileByUuid(String uuid, String suffix) {
        this.xmlFiles.remove(getXmlFileForUuid(uuid, suffix));
    }

    public boolean containsXmlFileForUuid(String uuid, String suffix) {
        return getXmlFileForUuid(uuid, suffix) != null;
    }
}
