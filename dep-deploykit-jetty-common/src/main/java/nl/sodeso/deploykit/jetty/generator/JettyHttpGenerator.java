package nl.sodeso.deploykit.jetty.generator;

import freemarker.template.Template;
import nl.sodeso.commons.freemarker.FreemarkerException;
import nl.sodeso.commons.freemarker.FreemarkerUtil;
import nl.sodeso.deploykit.jetty.ErrorCode;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.jetty.deploy.DeployException;
import nl.sodeso.deploykit.service.model.connectors.Http;

import javax.annotation.Nonnull;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Ronald Mathies
 */
public class JettyHttpGenerator extends AbstractGenerator {

    private static final Logger LOG = Logger.getLogger(JettyHttpGenerator.class.getName());

    public static void generate(@Nonnull Http http, @Nonnull JettyXmlFile jettyXmlFile) {
        try {
            LOG.log(Level.INFO, "Generating HTTP configuration '" + http.getLabel() + "'.");
            Template template = FreemarkerUtil.loadTemplate("jetty-http.ftl");
            FreemarkerUtil.process(template, http, jettyXmlFile.getXmlFile());

            LOG.log(Level.INFO, "Finished generating HTTP configuration '" + http.getLabel() + "'.");
        } catch (FreemarkerException e) {
            throw new DeployException(ErrorCode.ERR_012, e.getMessage());
        }
    }
}
