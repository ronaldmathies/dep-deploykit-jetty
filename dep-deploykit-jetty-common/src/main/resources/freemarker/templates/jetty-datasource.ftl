<?xml version="1.0"?>
<!DOCTYPE Configure PUBLIC "-//Jetty//Configure//EN" "http://www.eclipse.org/jetty/configure.dtd">

<Configure id="Server" class="org.eclipse.jetty.server.Server">
    <New id="${uuid}" class="org.eclipse.jetty.plus.jndi.Resource">
        <Arg>${jndi}</Arg>
        <Arg>
            <New class="org.apache.commons.dbcp2.BasicDataSource">
                <Set name="driverClassName">${jdbcDriver.jdbcDriverClass}</Set>
                <Set name="url">${url}</Set>
                <Set name="username">${username}</Set>
                <Set name="password">${password}</Set>

                <!-- Transaction Properties -->
                <Set name="defaultCatalog">${jettyConnectionPool.defaultCatalog}</Set>
<#if jettyConnectionPool.defaultAutoCommit = 0>
                <Set name="defaultAutoCommit">false</Set>
<#elseif jettyConnectionPool.defaultAutoCommit = 1>
                <Set name="defaultAutoCommit">true</Set>
</#if>
<#if jettyConnectionPool.defaultReadOnly = 0>
                <Set name="defaultReadOnly">false</Set>
<#elseif jettyConnectionPool.defaultReadOnly = 1>
                <Set name="defaultReadOnly">true</Set>
</#if>
<#if jettyConnectionPool.defaultTransactionIsolation?has_content>
                <Set name="defaultTransactionIsolation">${jettyConnectionPool.defaultTransactionIsolation}</Set>
</#if>
<#if jettyConnectionPool.cacheState = false>
                <Set name="cacheState">false</Set>
<#elseif jettyConnectionPool.cacheState = true>
                <Set name="cacheState">true</Set>
</#if>

                <!-- Connection Pool Properties -->
                <Set name="initialSize">${jettyConnectionPool.initialSize?c}</Set>
                <Set name="maxTotal">${jettyConnectionPool.maxTotal?c}</Set>
                <Set name="maxIdle">${jettyConnectionPool.maxIdle?c}</Set>
                <Set name="minIdle">${jettyConnectionPool.minIdle?c}</Set>
                <Set name="maxWaitMillis">${jettyConnectionPool.maxWaitMillis?c}</Set>

                <!-- Connection Properties -->
                <Set name="validationQuery">${jettyConnectionPool.validationQuery}</Set>
<#if jettyConnectionPool.testOnCreate = false>
                <Set name="testOnCreate">false</Set>
<#elseif jettyConnectionPool.testOnCreate = true>
                <Set name="testOnCreate">true</Set>
</#if>
<#if jettyConnectionPool.testOnBorrow = false>
                <Set name="testOnBorrow">false</Set>
<#elseif jettyConnectionPool.testOnBorrow = true>
                <Set name="testOnBorrow">true</Set>
</#if>
<#if jettyConnectionPool.testOnReturn = false>
                <Set name="testOnReturn">false</Set>
<#elseif jettyConnectionPool.testOnReturn = true>
                <Set name="testOnReturn">true</Set>
</#if>
<#if jettyConnectionPool.testWhileIdle = false>
                <Set name="testWhileIdle">false</Set>
<#elseif jettyConnectionPool.testWhileIdle = true>
                <Set name="testWhileIdle">true</Set>
</#if>
                <Set name="timeBetweenEvictionRunsMillis">${jettyConnectionPool.timeBetweenEvictionRunsMillis?c}</Set>
                <Set name="numTestsPerEvictionRun">${jettyConnectionPool.numTestsPerEvictionRun?c}</Set>
                <Set name="minEvictableIdleTimeMillis">${jettyConnectionPool.minEvictableIdleTimeMillis?c}</Set>
                <Set name="softMiniEvictableIdleTimeMillis">${jettyConnectionPool.softMiniEvictableIdleTimeMillis?c}</Set>
                <Set name="maxConnLifetimeMillis">${jettyConnectionPool.maxConnLifetimeMillis?c}</Set>
<#if jettyConnectionPool.logExpiredConnections = false>
                <Set name="logExpiredConnections">false</Set>
<#elseif jettyConnectionPool.logExpiredConnections = true>
                <Set name="logExpiredConnections">true</Set>
</#if>
<#if jettyConnectionPool.connectionInitSqls?has_content>
                <Set name="connectionInitSqls">${jettyConnectionPool.connectionInitSqls}</Set>
</#if>
<#if jettyConnectionPool.lifo = false>
                <Set name="lifo">false</Set>
<#elseif jettyConnectionPool.lifo = true>
                <Set name="lifo">true</Set>
</#if>
            </New>
        </Arg>

    </New>
</Configure>