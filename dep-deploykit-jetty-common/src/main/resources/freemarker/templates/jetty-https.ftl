<?xml version="1.0"?>
<!DOCTYPE Configure PUBLIC "-//Jetty//Configure//EN" "http://www.eclipse.org/jetty/configure_9_0.dtd">

<!-- ============================================================= -->
<!-- Configure a HTTPS connector.                                  -->
<!-- This configuration must be used in conjunction with jetty.xml -->
<!-- and jetty-ssl.xml.                                            -->
<!-- ============================================================= -->
<Configure id="Server" class="org.eclipse.jetty.server.Server">

    <!-- =========================================================== -->
    <!-- Add a HTTPS Connector.                                      -->
    <!-- Configure an o.e.j.server.ServerConnector with connection   -->
    <!-- factories for TLS (aka SSL) and HTTP to provide HTTPS.      -->
    <!-- All accepted TLS connections are wired to a HTTP connection.-->
    <!--                                                             -->
    <!-- Consult the javadoc of o.e.j.server.ServerConnector,        -->
    <!-- o.e.j.server.SslConnectionFactory and                       -->
    <!-- o.e.j.server.HttpConnectionFactory for all configuration    -->
    <!-- that may be set here.                                       -->
    <!-- =========================================================== -->
    <Call id="httpsConnector" name="addConnector">
        <Arg>
            <New class="org.eclipse.jetty.server.ServerConnector">
                <Arg name="server"><Ref refid="Server" /></Arg>
                <Arg name="factories">
                    <Array type="org.eclipse.jetty.server.ConnectionFactory">
                        <Item>
                            <New class="org.eclipse.jetty.server.SslConnectionFactory">
                                <Arg name="next">http/1.1</Arg>
                                <Arg name="sslContextFactory"><Ref refid="sslContextFactory"/></Arg>
                            </New>
                        </Item>
                        <Item>
                            <New class="org.eclipse.jetty.server.HttpConnectionFactory">
                                <Arg name="config"><Ref refid="sslHttpConfig"/></Arg>
                            </New>
                        </Item>
                    </Array>
                </Arg>
<#if host?has_content>
                <Set name="host"><Property name="jetty.host" default="${host}"/></Set>
<#else >
                <Set name="host"><Property name="jetty.host"/></Set>
</#if>
                <Set name="port"><Property name="https.port" default="${port?c}" /></Set>
                <Set name="idleTimeout"><Property name="https.timeout" default="${timeout?c}"/></Set>
            </New>
        </Arg>
    </Call>

    <!-- ============================================================= -->
    <!-- Configure a TLS (SSL) Context Factory                         -->
    <!-- This configuration must be used in conjunction with jetty.xml -->
    <!-- and either jetty-https.xml or jetty-spdy.xml (but not both)   -->
    <!-- ============================================================= -->
    <New id="sslContextFactory" class="org.eclipse.jetty.util.ssl.SslContextFactory">
        <Set name="KeyStorePath"><Property name="jetty.base" default="." />/<Property name="jetty.keystore" default="${uuid}.keystore"/></Set>
        <Set name="KeyStorePassword"><Property name="jetty.keystore.password" default="${keyStorePassword}"/></Set>
        <Set name="KeyManagerPassword"><Property name="jetty.keymanager.password" default="${keyManagerPassword}"/></Set>
        <Set name="TrustStorePath"><Property name="jetty.base" default="." />/<Property name="jetty.truststore" default="${uuid}.keystore"/></Set>
        <Set name="TrustStorePassword"><Property name="jetty.truststore.password" default="${trustStorePassword}"/></Set>
        <Set name="EndpointIdentificationAlgorithm"></Set>
        <Set name="ExcludeCipherSuites">
            <Array type="String">
                <Item>SSL_RSA_WITH_NULL_MD5</Item>
                <Item>SSL_RSA_WITH_NULL_SHA</Item>
                <Item>SSL_RSA_EXPORT_WITH_RC4_40_MD5</Item>
                <Item>SSL_RSA_WITH_RC4_128_MD5</Item>
                <Item>SSL_RSA_WITH_RC4_128_SHA</Item>
                <Item>SSL_RSA_EXPORT_WITH_RC2_CBC_40_MD5</Item>
                <Item>SSL_RSA_WITH_IDEA_CBC_SHA</Item>
                <Item>SSL_RSA_EXPORT_WITH_DES40_CBC_SHA</Item>
                <Item>SSL_RSA_WITH_DES_CBC_SHA</Item>
                <Item>SSL_RSA_WITH_3DES_EDE_CBC_SHA</Item>
                <Item>SSL_DH_DSS_EXPORT_WITH_DES40_CBC_SHA</Item>
                <Item>SSL_DH_DSS_WITH_DES_CBC_SHA</Item>
                <Item>SSL_DH_DSS_WITH_3DES_EDE_CBC_SHA</Item>
                <Item>SSL_DH_RSA_EXPORT_WITH_DES40_CBC_SHA</Item>
                <Item>SSL_DH_RSA_WITH_DES_CBC_SHA</Item>
                <Item>SSL_DH_RSA_WITH_3DES_EDE_CBC_SHA</Item>
                <Item>SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA</Item>
                <Item>SSL_DHE_DSS_WITH_DES_CBC_SHA</Item>
                <Item>SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA</Item>
                <Item>SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA</Item>
                <Item>SSL_DHE_RSA_WITH_DES_CBC_SHA</Item>
                <Item>SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA</Item>
                <Item>SSL_DH_anon_EXPORT_WITH_RC4_40_MD5</Item>
                <Item>SSL_DH_anon_WITH_RC4_128_MD5</Item>
                <Item>SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA</Item>
                <Item>SSL_DH_anon_WITH_DES_CBC_SHA</Item>
                <Item>SSL_DH_anon_WITH_3DES_EDE_CBC_SHA</Item>
                <Item>SSL_FORTEZZA_KEA_WITH_NULL_SHA</Item>
                <Item>SSL_FORTEZZA_KEA_WITH_FORTEZZA_CBC_SHA</Item>
                <Item>SSL_FORTEZZA_KEA_WITH_RC4_128_SHA</Item>
                <Item>SSL_DHE_RSA_WITH_AES_128_CBC_SHA</Item>
                <Item>SSL_RSA_WITH_AES_128_CBC_SHA</Item>
            </Array>
        </Set>
        <Set name="ExcludeProtocols">
            <Array type="String">
                <Item>SSLv3</Item>
            </Array>
        </Set>
    </New>

    <New id="httpConfig" class="org.eclipse.jetty.server.HttpConfiguration">
        <Set name="secureScheme">https</Set>
        <Set name="securePort">
            <Property name="jetty.secure.port" default="${port?c}" />
        </Set>
        <Set name="outputBufferSize">32768</Set>
        <Set name="outputAggregationSize">8192</Set>
        <Set name="requestHeaderSize">8192</Set>
        <Set name="responseHeaderSize">8192</Set>
        <Set name="sendServerVersion">true</Set>
        <Set name="sendDateHeader">false</Set>
        <Set name="headerCacheSize">512</Set>
        <Set name="delayDispatchUntilContent">false</Set>
    </New>

    <!-- =========================================================== -->
    <!-- Create a TLS specific HttpConfiguration based on the        -->
    <!-- common HttpConfiguration defined in jetty.xml               -->
    <!-- Add a SecureRequestCustomizer to extract certificate and    -->
    <!-- session information                                         -->
    <!-- =========================================================== -->
    <New id="sslHttpConfig" class="org.eclipse.jetty.server.HttpConfiguration">
        <Arg><Ref refid="httpConfig"/></Arg>
        <Call name="addCustomizer">
            <Arg><New class="org.eclipse.jetty.server.SecureRequestCustomizer"/></Arg>
        </Call>
    </New>

</Configure>