${realm} {
org.eclipse.jetty.jaas.spi.LdapLoginModule required
debug="true"
contextFactory="com.sun.jndi.ldap.LdapCtxFactory"
hostname="${ldapService.domain}"
port="${ldapService.port?c}"
bindDn="${ldapService.username}"
bindPassword="${ldapService.password}"
authenticationMethod="simple"
<#if forceBindingLogin = false>
forceBindingLogin="false"
<#elseif forceBindingLogin = true>
forceBindingLogin="true"
</#if>
userBaseDn="${userBaseDn}"
userRdnAttribute="${userAttribute}"
userIdAttribute="${userAttribute}"
userPasswordAttribute="${userPasswordAttribute}"
userObjectClass="${userObjectClass}"
roleBaseDn="${roleBaseDn}"
roleNameAttribute="${roleNameAttribute}"
roleMemberAttribute="${roleMemberAttribute}"
roleObjectClass="${roleObjectClass}";
};