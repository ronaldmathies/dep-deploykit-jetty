package nl.sodeso.deploykit.jetty.generator;

import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.service.model.accesscontrol.LdapAccessControl;
import nl.sodeso.deploykit.service.model.services.ldap.LdapService;
import org.junit.Test;

/**
 * @author Ronald Mathies
 */
public class JettyLdapAuthenticationGeneratorTest extends AbstractGeneratorTest {

    @Test
    public void testGenerate() throws Exception {
        LdapAccessControl ldapAccessControl = new LdapAccessControl();
        ldapAccessControl.setUuid("ldap-config");
        ldapAccessControl.setLabel("Label");
        ldapAccessControl.setRealm("myRealm");
        ldapAccessControl.setPlaceholderPrefix("ldap-ac-prefix");

        ldapAccessControl.setUserBaseDn("ou=users,dc=example,dc=com");
        ldapAccessControl.setUserAttribute("uid");
        ldapAccessControl.setUserPasswordAttribute("userPassword");
        ldapAccessControl.setUserObjectClass("person");

        ldapAccessControl.setRoleBaseDn("ou=roles,dc=example,dc=com");
        ldapAccessControl.setRoleMemberAttribute("member");
        ldapAccessControl.setRoleNameAttribute("cn");
        ldapAccessControl.setRoleObjectClass("groupOfNames");
        ldapAccessControl.setForceBindingLogin(true);

        LdapService ldapService = new LdapService();
        ldapService.setDomain("192.168.99.100");
        ldapService.setPort(389);
        ldapService.setPlaceholderPrefix("ldap-service-prefix");
        ldapService.setUsername("uid=admin,ou=system");
        ldapService.setPassword("secret");

        ldapAccessControl.setLdapService(ldapService);

        JettyXmlFile jettyXmlFile = new JettyXmlFile(ldapAccessControl.getUuid(), "ldap");
        JettyLdapAuthenticationGenerator.generate(ldapAccessControl, jettyXmlFile);

        compareTwoFiles("src/test/resources/ldap-config-ldap.xml", "target/etc/ldap-config-ldap.xml");
        compareTwoFiles("src/test/resources/login.conf", "target/etc/login.conf");

    }
}