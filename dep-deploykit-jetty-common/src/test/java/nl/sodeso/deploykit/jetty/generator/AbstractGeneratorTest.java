package nl.sodeso.deploykit.jetty.generator;

import nl.sodeso.commons.fileutils.DigestUtil;
import nl.sodeso.commons.fileutils.exception.DigestException;
import nl.sodeso.deploykit.jetty.util.SystemPropertiesContainer;
import org.junit.BeforeClass;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * @author Ronald Mathies
 */
public abstract class AbstractGeneratorTest {

    @BeforeClass
    public static void init() {
        String etcPath = System.getProperty("user.dir") + "/target";

        System.setProperty("deploykit.home", System.getProperty("user.dir") + "/src/test/resources");
        System.setProperty(SystemPropertiesContainer.JETTY_HOME, etcPath);

        File etcFile = new File(etcPath + "/etc");
        if (!etcFile.exists()) {
            etcFile.mkdirs();
        }
    }

    protected void compareTwoFiles(String file1, String file2) throws DigestException {
        String digestFromFile1 = DigestUtil.createDigest(file1);
        String digestFromFile2 = DigestUtil.createDigest(file2);

        assertEquals(digestFromFile1, digestFromFile2);
    }

}
