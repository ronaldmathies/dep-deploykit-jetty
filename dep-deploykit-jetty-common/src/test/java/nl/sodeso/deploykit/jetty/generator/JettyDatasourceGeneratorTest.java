package nl.sodeso.deploykit.jetty.generator;

import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.service.model.datasource.connectionpool.JettyConnectionPool;
import nl.sodeso.deploykit.service.model.datasource.datasource.Datasource;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriver;
import nl.sodeso.deploykit.service.model.datasource.jdbcdriver.JdbcDriverJar;
import org.junit.Test;

/**
 * @author Ronald Mathies
 */
public class JettyDatasourceGeneratorTest extends AbstractGeneratorTest {

    @Test
    public void testGenerate() throws Exception {
        Datasource datasource = new Datasource();
        datasource.setUuid("mysql_datasource");
        datasource.setLabel("My MySql Datasource");
        datasource.setUrl("jdbc:mysql://192.168.99.100");
        datasource.setJndi("jdbc/mysql");
        datasource.setUsername("username");
        datasource.setPassword("password");

        JdbcDriverJar jdbcDriverJar = new JdbcDriverJar();
        jdbcDriverJar.setFilename("driver.jar");

        JdbcDriver jdbcDriver = new JdbcDriver();
        jdbcDriver.getJdbcDriverJars().add(jdbcDriverJar);
        jdbcDriver.setJdbcDriverClass("com.mysql.jdbc.Driver");
        datasource.setJdbcDriver(jdbcDriver);

        JettyConnectionPool jettyConnectionPool = new JettyConnectionPool();
        jettyConnectionPool.setDefaultCatalog("catalog");
        jettyConnectionPool.setDefaultAutoCommit(0);
        jettyConnectionPool.setDefaultReadOnly(0);
        jettyConnectionPool.setDefaultTransactionIsolation("DEFAULT");
        jettyConnectionPool.setCacheState(true);
        jettyConnectionPool.setTestOnCreate(true);
        jettyConnectionPool.setTestOnBorrow(true);
        jettyConnectionPool.setTestOnReturn(true);
        jettyConnectionPool.setTestWhileIdle(true);
        jettyConnectionPool.setLogExpiredConnections(false);
        jettyConnectionPool.setLifo(true);
        jettyConnectionPool.setCacheState(true);
        jettyConnectionPool.setInitialSize(5);
        jettyConnectionPool.setMaxTotal(10);
        jettyConnectionPool.setMaxIdle(8);
        jettyConnectionPool.setMinIdle(7);
        jettyConnectionPool.setMaxWaitMillis(1000);
        jettyConnectionPool.setValidationQuery("SELECT COUNT(ID) FROM TABLE");
        jettyConnectionPool.setTimeBetweenEvictionRunsMillis(1000);
        jettyConnectionPool.setNumTestsPerEvictionRun(2);
        jettyConnectionPool.setMinEvictableIdleTimeMillis(1000);
        jettyConnectionPool.setSoftMiniEvictableIdleTimeMillis(1000);
        jettyConnectionPool.setMaxConnLifetimeMillis(1000);
        jettyConnectionPool.setLogExpiredConnections(true);
        jettyConnectionPool.setConnectionInitSqls("connectionInitSqls");

        datasource.setJettyConnectionPool(jettyConnectionPool);

        JettyXmlFile jettyXmlFile = new JettyXmlFile(datasource.getUuid(), "ds");
        JettyDatasourceGenerator.generate(datasource, jettyXmlFile);

        compareTwoFiles("src/test/resources/mysql_datasource-ds.xml", "target/etc/mysql_datasource-ds.xml");
    }

}