package nl.sodeso.deploykit.jetty.generator;

import nl.sodeso.commons.fileutils.exception.DigestException;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.service.model.connectors.Http;
import org.junit.Test;

/**
 * @author Ronald Mathies
 */
public class JettyHttpGeneratorTest extends AbstractGeneratorTest {

    @Test
    public void testGenerator() throws DigestException {
        Http http = new Http();
        http.setUuid("http-config");
        http.setLabel("http-config");
        http.setTimeout(60000);
        http.setHost("127.0.0.1");
        http.setPort(8080);

        JettyXmlFile jettyXmlFile = new JettyXmlFile(http.getUuid(), "http");
        JettyHttpGenerator.generate(http, jettyXmlFile);

        compareTwoFiles("src/test/resources/http-config-http.xml", "target/etc/http-config-http.xml");
    }

}
