package nl.sodeso.deploykit.jetty.generator;

import nl.sodeso.commons.fileutils.exception.DigestException;
import nl.sodeso.deploykit.jetty.configuration.JettyXmlFile;
import nl.sodeso.deploykit.service.builder.util.Base64Util;
import nl.sodeso.deploykit.service.model.connectors.Https;
import org.junit.Test;

import java.io.InputStream;

/**
 * @author Ronald Mathies
 */
public class JettyHttpsGeneratorTest extends AbstractGeneratorTest {

    @Test
    public void testGenerator() throws DigestException {
        Https https = new Https();
        https.setUuid("ssl-config");
        https.setKeyManagerPassword("keymanagerpass");
        https.setTrustStorePassword("truststorepass");
        https.setKeyStorePassword("keystorepass");
        https.setLabel("ssl-config");
        https.setTimeout(60000);
        https.setPort(8080);

        InputStream is = JettyHttpsGenerator.class.getResourceAsStream("/ssl-config.keystore");
        https.setKeystore(Base64Util.to(is).toString());

        JettyXmlFile jettyXmlFile = new JettyXmlFile(https.getUuid(), "ssl");
        JettyHttpsGenerator.generate(https, jettyXmlFile);

        compareTwoFiles("src/test/resources/ssl-config.keystore", "target/etc/ssl-config.keystore");
        compareTwoFiles("src/test/resources/ssl-config-ssl.xml", "target/etc/ssl-config-ssl.xml");
    }

}
