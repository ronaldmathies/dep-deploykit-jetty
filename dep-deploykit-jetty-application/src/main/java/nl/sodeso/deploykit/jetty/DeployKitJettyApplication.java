package nl.sodeso.deploykit.jetty;

import nl.sodeso.deploykit.jetty.deploy.DeployException;

/**
 * @author Ronald Mathies
 */
public class DeployKitJettyApplication {

    public static void main(String args[]) {
        try {
            DeployProfile.deploy();
            System.exit(0);
        } catch (DeployException e) {
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }

}